# es6-loops-speed

This script compares the speed of various common array iteration techniques in node.js.

The resulting data can be found [here](https://plot.ly/~juravenator/4)
