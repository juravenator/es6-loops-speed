#!/usr/bin/env node
require('dotenv').config()
require('console.table');
var readline = require('readline');

var makeTestArray = size => Array(size).fill(0).map( (node, i) => i )

var loops = {
  map: (array, mapFunction) => array.map(mapFunction),
  forEach: (array, mapFunction) => {
    var result = [];
    array.forEach( (item, i, array) => result.push(mapFunction(item, i, array)) );
    return result;
  },
  forLoop: (array, mapFunction) => {
    var result = [];
    for (var i = 0; i < array.length; i++) {
      result.push(mapFunction(array[i], i, array));
    }
    return result;
  },
  forLoopOptimised: (array, mapFunction) => {
    var arrayLen = array.length;
    var result = new Array(arrayLen);
    for(var i = 0; i < arrayLen; i++) {
      result[i] = mapFunction(array[i], i, array);
    }
    return result;
  }
}
var loopOperations = {
  number: (x,y,z) => x*y,
  string: (x,y,z) => (x.toString() + Math.random().toString())
}

var hrtime = process.hrtime;

var increment = 1;
var max = 1000;

var test = (array, testfn, operation) => {
  var t = hrtime();
  testfn(array, operation);
  t = hrtime(t);
  return `${t[0]}.${t[1]}`;
}

var result = [];

for (var i = increment; i <= max; i+=increment) {
  let array = makeTestArray(i);
  readline.clearLine(process.stdout, 0);
  readline.cursorTo(process.stdout, 0);
  let percent = Math.round(i/max*1000)/10;
  process.stdout.write(`testing ... ${percent}%`);
  result.push({
    size: i,
    ["numbers map"]: test(array, loops.map, loopOperations.number),
    ["numbers forEach"]: test(array, loops.forEach, loopOperations.number),
    ["numbers for loop"]: test(array, loops.forLoop, loopOperations.number),
    ["numbers optimized for loop"]: test(array, loops.forLoopOptimised, loopOperations.number),
    ["strings map"]: test(array, loops.map, loopOperations.string),
    ["strings forEach"]: test(array, loops.forEach, loopOperations.string),
    ["strings for loop"]: test(array, loops.forLoop, loopOperations.string),
    ["strings optimized for loop"]: test(array, loops.forLoopOptimised, loopOperations.string),
  })
}
console.log("");
// console.table(result);

var plotly = require('plotly')(process.env.PLOTLY_USERNAME, process.env.PLOTLY_API_KEY);
var plotlyData = result.reduce( (result, row) => {
  Object.keys(row).filter(key => key != "size").forEach( key => {
    var o = result.find( k => k.name == key) || result[result.push({
      name: key,
      mode: "lines+markers",
      type: "scatter",
      x: [],
      y: []
    })-1];
    o.x.push(row.size);
    o.y.push(row[key]);
  })
  return result;
}, []);
plotly.plot(plotlyData, {
  filename: "es6-loops",
  fileopt: "overwrite",
  layout: {
    title: `Speed comparison of various array iterators in node v${process.versions.node}`,
    xaxis: {
      title: "array size"
    },
    yaxis: {
      title: "time to full iteration"
    }
  }
}, (err, msg) => {
  if (err) {
    console.error("error while instructing plotly", err);
  }
  console.log("plotly response", msg);
});
